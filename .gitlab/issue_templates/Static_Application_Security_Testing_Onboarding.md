# Onboarding Issue: SAST and Static Analysis Foundations

## 1. SAST Overview

### Watch the SAST engineering guide
Get oriented with SAST at GitLab.

- [ ] [SAST Engineering Guide](https://www.youtube.com/watch?v=yszD02ENu2U) ([slides](https://docs.google.com/presentation/d/1jlo1FEMS5qUwB_nCffSWmU_5mweZMyFvKXnnbtwIDqc/edit?usp=sharing))

### Overview of Advanced SAST capabilities
Learn about the features of Gitlab Advanced SAST
- [ ] [GitLab Advanced SAST: Accelerating Vulnerability Resolution](https://www.youtube.com/watch?v=xDa1MHOcyn8)

## 2. Foundational Materials: Static Analysis Concepts

The following sections provide a theoretical foundation for static analysis and
program analysis, which are crucial for understanding how [GitLab Advanced SAST
(GLAS)](https://docs.gitlab.com/ee/user/application_security/sast/gitlab_advanced_sast.html)
operates. Please go through these materials as they will help you grasp the core
principles.

1. **Introduction into Compiler Engineering/Program analysis**
   1. [ ] [Intro](https://drive.google.com/file/d/16qNJGI4BayhISVGxKhUgMPcozURtryks/view?usp=drive_link)
   1. [ ] [Compiler Pipeline](https://drive.google.com/file/d/1RLMiSXn_zuOpmKPPy5k7bW_QUqTz9X_2/view?usp=drive_link)
   1. [ ] [Introduction to Formal Languages](https://drive.google.com/file/d/1yROjwi9h6jzBMmsCdPnguOY_kWbKsxRU/view?usp=drive_link)
   1. **From Context-Free Grammars to Parsing**
      1. [ ] [Context-Free Grammars (Definition)](https://drive.google.com/file/d/1B2w7_rgZVTnVe-iOvGJ23guvYVIyqIcc/view?usp=drive_link)
      1. [ ] [Context-Free Grammars (Application)](https://drive.google.com/file/d/125dv2D5AMNFdF56MpMQw-fG4-oMgWiZA/view?usp=drive_link)
      1. [ ] [Context-Free Grammars (Ambiguities)](https://drive.google.com/file/d/1YhkgA6xOofbBPBv2-m-mg0ezdlVkm3Zz/view?usp=drive_link)
      1. [ ] [Bottom-Up Parsing](https://drive.google.com/file/d/1QDeFBwgZEQL9kHmLHVIXplKLFSYHUD__/view?usp=sharing)
      1. [ ] [Grammar Design (Example: Operator Precedence)](https://drive.google.com/file/d/1lqikw-6klM--SDo-UC_RNthlvKb0Rwl1/view?usp=drive_link)
      1. [ ] [Syntax Tree Processing/Traversal](https://drive.google.com/file/d/1LC0Y_Yks75n_MItsP0K6kDqwm6tm-99-/view?usp=drive_link)
      1. [ ] [Left Recursion (Elimination)](https://drive.google.com/file/d/1g12PvGHjVgBWP3SXGaW1Hnez34D63g84/view?usp=drive_link)
   1. **Semantics**
      1. [ ] [Intro](https://drive.google.com/file/d/1GyGGlOr8_oeFP5HQl8O6KM4_9hFm07i4/view?usp=drive_link)
      1. [ ] [Typechecking](https://drive.google.com/file/d/1rQFexZFzjqojI_M4ihSuLEMx3hGkYymn/view?usp=drive_link)
      1. [ ] [Operational Semantics (Small Step)](https://drive.google.com/file/d/1Ci_tL1bYXlAjhwgmR8vuvTuNinDAio7Z/view?usp=drive_link)
1. **Hands-on Experiment/Project (propositional formula evaluator (PFE))**
   1. [ ] [Intro](https://drive.google.com/file/d/1JGzu2CLn_fgbtJFNmoexW8wsAqvJcrgh/view?usp=drive_link)
   1. **Design**
      1. [ ] [Grammar Definition](https://drive.google.com/file/d/17vla5QKlh8058v_bYyzxz-Aqt-jakEIv/view?usp=drive_link) ([Exercise](https://gitlab.com/gitlab-org/gitlab/-/issues/408963#note_1859569874))
      1. [ ] [Type Checker](https://drive.google.com/file/d/18WW949tu2w7iWmTEmyXsLF5eG7bGKiA2/view?usp=drive_link)
      1. [ ] [Semantic Rules](https://drive.google.com/file/d/1s8XPWKiGeT-Hs3-1O-jscw79PvnengRh/view?usp=sharing)
   1. **Implementation**
      1. [ ] [Intro](https://drive.google.com/file/d/15xDfLZ80-Owhbqlrn21eL3ujKPyriteC/view?usp=drive_link) ([Code](https://gitlab.com/gitlab-org/secure/vulnerability-research/vet/onboarding/pfe))
      1. [ ] [Coding Exercise](https://drive.google.com/file/d/1hMpbe7GY4Zu48P6wTlSn9euhbUbANRU7/view?usp=drive_link) ([Exercise](https://gitlab.com/gitlab-org/gitlab/-/issues/408963#note_1868057289))

## 3. GLAS: Introduction

GLAS is a key component of our static analysis strategy. To contribute
effectively, familiarity with OCaml and GLAS, is essential.

### 3.1 OCaml Setup

If you're already familiar with OCaml, you can skip this section.

To get started with OCaml, please follow these steps to set up your environment.
This will be crucial for working with GLAS.

1. [ ] **Install the OCaml Package Manager:**
   ```shell
   brew install opam
   ```
2. [ ] **Create an Opam Switch with OCaml 5.1.0:**
   ```shell
   opam init
   opam switch create fun-chess 5.1.0
   eval $(opam env --switch=fun-chess)
   ```
3. [ ] **Install VSCode Extension:**
   - [OCaml Platform](https://marketplace.visualstudio.com/items?itemName=ocamllabs.ocaml-platform)
   ```shell
   opam install ocaml-lsp-server
   opam install ocamlformat
   ```
4. [ ] **Install UTop (Enhanced OCaml REPL):**
   ```shell
   opam install utop
   ```
5. [ ] **Install Additional Dependencies:**
   ```shell
   opam install dune merlin odoc dune-release
   ```

## 4. Hands-On Coding Practice: OCaml Chess Bot

If you're already familiar with OCaml, you can skip this section.

Create an OCaml chess bot to practice both OCaml and functional programming.
Even though the tournament has already passed and participation in the full
workshop is optional, building the chess bot will give you valuable hands-on
experience with the language and tools used in GLAS development.

### 4.1 Instructions for Building a Chess Bot

1. [ ] **Watch the videos**:
   - [ ] [Fun Programming Workshop - OCaml & Chess (part 1)](https://www.youtube.com/watch?v=NkmQ-jGwotE)
   - [ ] [OCaml Chess-Bot Tournament](https://www.youtube.com/watch?v=POV9dw16_sA)

1. [ ] **Clone the Repository**:
   ```shell
   git clone https://gitlab.com/mbenayoun/OCaml-Chess-Tournament
   ```

1. [ ] **Run the Game**:
   ```shell
   sh chess.sh
   ```
1. **Create Your Bot**:
   - [ ] Add a bot to the list of bots in the repo.
   - [ ] Implement your bot using OCaml.

1. **Test Your Bot Locally**:
   - [ ] Play against your own bot to test and improve your algorithm.

1. **Submit a Merge Request**:
   - [ ] Submit your implementation via an MR.
   - [ ] Get your code reviewed for feedback.

For any questions, feel free to reach out through the dedicated issue:
https://gitlab.com/gitlab-org/gitlab/-/issues/468970+s or in
[`#fp`](https://app.slack.com/client/E03N1RJJX7C/CT4KCM4E6) on Slack.

## 5. Watch GLAS Deep Dive Videos

Watch the GLAS Deep Dive video series to understand how GLAS works.

- [ ] [Part 1](https://www.youtube.com/watch?v=eWp3JK4XgOs)
- [ ] [Part 2](https://www.youtube.com/watch?v=tJCLcsAPc0U)

## 6. Clone and Compile GLAS

Clone and compile each project using the repository links below. Detailed
compilation instructions can be found in each project's README.

- [ ] [GitLab Advanced SAST](https://gitlab.com/gitlab-org/security-products/analyzers/gitlab-advanced-sast)
- [ ] [Lightz-AIO (scanner)](https://gitlab.com/gitlab-org/security-products/oxeye/product/lightz-aio)
- [ ] [Lightz (scan engine)](https://gitlab.com/gitlab-org/security-products/oxeye/product/lightz)

### 6.1 Choose an Onboarding Issue

- [ ] Visit the [onboarding issues list](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=onboarding&label_name%5B%5D=group%3A%3Astatic%20analysis&label_name%5B%5D=AdvancedSast%3A%3A%2a&first_page_size=20) to find issues labeled onboarding that are suitable for new team members. These tasks provide hands-on experience and allow you to contribute meaningful improvements from the start.

## 7. Resources and Additional Information

- [Static Analysis Handbook Page](https://handbook.gitlab.com/handbook/engineering/development/sec/secure/static-analysis/)
- [Static Analysis Group Priorities](https://about.gitlab.com/direction/secure/static-analysis/)
- [OCaml Official Website](https://ocaml.org/)
- [OCaml Chess Bot Repo](https://gitlab.com/mbenayoun/OCaml-Chess-Tournament)

### 7.1 Repositories
#### Analyzers
- [kubesec](https://gitlab.com/gitlab-org/security-products/analyzers/kubesec)
- [pmd-apex](https://gitlab.com/gitlab-org/security-products/analyzers/pmd-apex)
- [semgrep](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep)
- [sobelow](https://gitlab.com/gitlab-org/security-products/analyzers/sobelow)
- [spotbugs](https://gitlab.com/gitlab-org/security-products/analyzers/spotbugs)
- [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules)
- [gitlab-advanced-sast](https://gitlab.com/gitlab-org/security-products/analyzers/gitlab-advanced-sast)
- [lightz-aio](https://gitlab.com/gitlab-org/security-products/oxeye/product/lightz-aio)
- [lightz](https://gitlab.com/gitlab-org/security-products/oxeye/product/lightz)
- [oxeye-rulez](https://gitlab.com/gitlab-org/security-products/oxeye/product/oxeye-rulez)

#### Tools
- [sast-analyzer-deps-bot](https://gitlab.com/gitlab-org/security-products/analyzers/sast-analyzer-deps-bot)
- [sast-ide-benchgen](https://gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen)

### 7.2 Generate sample files for SAST scans
1. Clone [sast-ide-benchgen](https://gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen)
1. Configure the `Lang` field in `config.toml` for the desired language
1. Configure the `NumOssSamplesToGenerate` field in `config.toml` for the desired number of sample files
1. Run the script to generate the sample files [fetched from top repositories](https://gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen#how-it-works).
    ```
    go run cmd/main.go oss
    ```
1. Copy the files from `generated_oss_samples` to your test project

/assign me
/label ~"Category:SAST" ~"group::static analysis" ~"devops::secure" ~"section::sec" ~"type::ignore" ~onboarding
