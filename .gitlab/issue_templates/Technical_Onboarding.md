### Overview

The [Sec Section](https://about.gitlab.com/direction/security/) encompasses the [Govern](https://about.gitlab.com/handbook/engineering/development/sec/govern/) and [Secure](https://about.gitlab.com/handbook/engineering/development/sec/secure/) stages.

The features provided by these teams are mostly leveraging tools that are executed during pipelines,
using Docker images.
That’s why it’s crucial to set up a development environment with the [GitLab CI Runner](https://gitlab.com/gitlab-org/gitlab-runner).

This document will guide you through the whole process. Don't hesitate to ask questions in Slack if anything is unclear:

- [`#sec-section`](https://gitlab.enterprise.slack.com/archives/C02087FTL5V) for things relevant to the Govern and Secure Stages
- [`#s_govern`](https://gitlab.enterprise.slack.com/archives/CFHGVJ06R) for things related to the Govern Stage
- [`#s_secure`](https://gitlab.enterprise.slack.com/archives/C8S0HHM44) for things related to the Secure Stage
- [`#questions`](https://gitlab.enterprise.slack.com/archives/C0AR2KW4B) for any general questions
- [`#gdk`](https://gitlab.enterprise.slack.com/archives/C2Z9A056E) for [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit) related questions
- [`#sec-section-social`](https://gitlab.enterprise.slack.com/archives/C01ACJRU5PH) to meet the rest of the section
- [`#sec-section-donut`](https://gitlab.enterprise.slack.com/archives/C04A2LM75FY) automated random coffee chat with another member of the channel every other week

Enjoy!

Need some help? Ping one of your [Sec team mates](https://about.gitlab.com/company/team/?department=sec-section), I'm sure they will be more than willing to answer your call!

#### General

1. [ ] Review the relevant handbook pages to understand how we fit in the organization structure, and what product categories we're responsible for developing and maintaining:
   1. [ ] [Direction for the Sec Section](https://about.gitlab.com/direction/security/)
   1. [ ] [Product Categories Hierarchy](https://about.gitlab.com/handbook/product/categories/#hierarchy) and find our Section, Stages, Groups and their respective Categories.
   1. [ ] [Secure and Govern Glossary of Terms](https://docs.gitlab.com/ee/user/application_security/terminology/) to get used to the terms you will frequently hear.

<details>
<summary>Govern</summary>

1. [ ] [Govern sub-department](https://about.gitlab.com/handbook/engineering/development/sec/govern/)
1. [ ] [Govern Stage](https://about.gitlab.com/handbook/product/categories/#govern-stage)
1. [ ] [Govern Stage Direction](https://about.gitlab.com/direction/govern/)


<details>
<summary>Threat Insights</summary>

1. [ ] Read the relevant user documentation for the Threat Insights categories:
   - Vulnerability Management
     1. [ ] [Secure and Govern terminology](https://docs.gitlab.com/ee/user/application_security/terminology/)
     1. [ ] [Security Dashboards](https://docs.gitlab.com/ee/user/application_security/security_dashboard/)
     1. [ ] (optional for frontend engineers) [Security scanner integration](https://docs.gitlab.com/ee/development/integrations/secure.html)
     1. [ ] [Vulnerability Page](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/)
     1. [ ] [Vulnerability Report](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/)
     1. [ ] [Vulnerability severity levels](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/severities.html)
   - Dependency Management
     1. [ ] [Dependency list](https://docs.gitlab.com/ee/user/application_security/dependency_list/)

</details>

<details>
<summary>Security Policies</summary>

- [ ] [Security Policies Group](https://about.gitlab.com/handbook/engineering/development/sec/govern/security-policies/)
- [ ] [Security Policies Group Direction](https://about.gitlab.com/direction/govern/security_policies/)
- [ ] Watch [Security Policies Vision](https://youtu.be/3poGbj--sNc)
- [ ] Read the relevant user documentation for the Security Policies group:
   - [ ] [Security Policies](https://docs.gitlab.com/ee/user/application_security/policies/)
      - [ ] [Scan execution policies](https://docs.gitlab.com/ee/user/application_security/policies/scan-execution-policies.html)
      - [ ] [Scan result policies](https://docs.gitlab.com/ee/user/application_security/policies/scan-result-policies.html)

</details>

<details>
<summary>Compliance</summary>

- [ ] [Compliance Group](https://about.gitlab.com/handbook/engineering/development/sec/govern/compliance/)
- [ ] [Compliance Group Direction](https://about.gitlab.com/direction/govern/compliance/)
- [ ] Read the relevant user documentation for the Compliance categories:
   - [ ] [Compliance Management](https://about.gitlab.com/direction/govern/compliance/compliance-management/)
     - [ ] [Compliance Frameworks](https://docs.gitlab.com/ee/user/group/compliance_frameworks.html#compliance-frameworks)
     - [ ] [Compliance Report](https://docs.gitlab.com/ee/user/compliance/compliance_report/index.html)
     - [ ]
   - [ ] [Audit Events](https://about.gitlab.com/direction/govern/compliance/audit-events/)
     - [ ] [Audit Events](https://docs.gitlab.com/ee/administration/audit_events.html)
     - [ ] [Audit Event Guide](https://docs.gitlab.com/ee/development/audit_event_guide/)
     - [ ] [Audit Reports](https://docs.gitlab.com/ee/administration/audit_reports.html)
     - [ ] [AUsit Event Streaming](https://docs.gitlab.com/ee/administration/audit_event_streaming.html)

</details>

</details>

<details>
<summary>Secure</summary>

1. [ ] Read about GitLab [Secure features, how they are set up and work](https://docs.gitlab.com/ee/user/application_security/)
   1. [ ] [Secure Stage](https://about.gitlab.com/handbook/product/categories/#secure-stage)
1. [ ] Optionally bookmark [repositories maintained by the Secure team](https://gitlab.com/gitlab-org/security-products).
1. [ ] The [Secure Group](https://gitlab.com/gitlab-org/secure) is another group which is used to address members of the Secure team and to keep other projects which do not directly affect the product. For instance the [onboarding](https://gitlab.com/gitlab-org/secure/onboarding).
1. [ ] Review the [OWASP Top 10 list](https://owasp.org/www-project-top-ten/). This will help understand the common security problems faced by web applications, something that the Secure group is trying to help our users with.

</details>

### Day 6: Setup

#### Accounts

1. [ ] New team member: Request a [self-hosted license](https://handbook.gitlab.com/handbook/engineering/developer-onboarding/#working-on-gitlab-ee-developer-licenses) to be able to work on GitLab EE features on your local development environment. Ensure to ask for an "Ultimate" license type, so that all features including the Security Dashboard are enabled. You should receive an email with a license file that you will use during GDK setup.
1. [ ] New Team member: Request a [gitlab.com ultimate license](https://handbook.gitlab.com/handbook/total-rewards/incentives/#gitlab-ultimate). This will help to validate example projects from onboarding as well as future development projects.
1. [ ] New team member: Create a new access request using the linked templates for the following items:
    - **Slack** only include the groups relevant to your team and/or position, not every group within your team's stage using the [Individual Bulk Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) template:
      - Govern: `@govern_security_policies_be`, `@govern_security_policies_fe`, `@govern_threat_insights_be`, `@govern_threat_insights_fe`
      - Secure: `@secure_composition_analysis_dev_team`, `@ast_dynamic_analysis_team`, `@secure_secret_detection_team`, `@secure_static_analysis_team`, `@secure_analyzer_frontend`
    - **Tableau** using [Tableau Request template](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues/new?issuable_template=Tableau_Request): request Viewer access to get used to the system and read the handbook about the different types of [licenses](https://handbook.gitlab.com/handbook/business-technology/data-team/platform/tableau/#tableau-licenses).
1. [ ] New team member: Sign-in on [Sentry](https://sentry.gitlab.net/gitlab/) and join the gitlab team.
1. [ ] New team member: Sign-in on [Grafana](https://dashboards.gitlab.net/). You can sign in with Google.
1. [ ] New team member: Say hello to the Sec team by introducing yourself on Slack channel #sec-section.

<details>
<summary>Govern</summary>

1. [ ] Manager: Add the new member to the [Govern Stage Calendar](https://calendar.google.com/calendar/u/0?cid=Z2l0bGFiLmNvbV9lZDYyMDd1ZWw3OGRlMGoxODQ5dmpqbmIza0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) and explain what it is.
1. [ ] Manager: Add the new team member to the [team based Google Group](https://groups.google.com/my-groups)
1. [ ] Manager: Add the new member to [https://staging.gitlab.com/govern-team-test](https://staging.gitlab.com/groups/govern-team-test/-/group_members) as Maintainer.
1. [ ] Manager: Add the new member to [https://gitlab.com/groups/gitlab-org/govern/demos/](https://gitlab.com/groups/gitlab-org/govern/demos/-/group_members) as `Maintainer`
1. [ ] Manager: Add the new member to the corresponding team's subgroup:
    - Security Policies Backend: https://gitlab.com/gitlab-org/govern/security-policies-backend as `Maintainer`
    - Security Policies Frontend: https://gitlab.com/gitlab-org/govern/security-policies-frontend as `Maintainer`
    - Threat Insights Backend: [`@gitlab-org/govern/threat-insights-backend-team`](https://gitlab.com/groups/gitlab-org/govern/threat-insights-backend-team/-/group_members?with_inherited_permissions=exclude) as  `Maintainer`
    - Threat Insights Frontend: [`@gitlab-org/govern/threat-insights-frontend-team`](https://gitlab.com/groups/gitlab-org/govern/threat-insights-frontend-team/-/group_members?with_inherited_permissions=exclude) as  `Maintainer`
1. [ ] Manager: Add to appropriate Slack channels
    - Engineering Slack `#sd_govern_engineering`
    - Frontend Slack `#sd_secure_govern_frontend`
    - Backend Slack `#sd_govern_backend`

<details>
<summary>Compliance</summary>

1. [ ] Manager: Add the new member to [https://staging.gitlab.com/compliance-tanuki](https://staging.gitlab.com/compliance-tanuki/-/group_members) as Maintainer.
1. [ ] Manager: Add the new member to [https://gitlab.com/sam-s-test-group](https://gitlab.com/sam-s-test-group/-/group_members) as Maintainer.
1. [ ] Manager: Add the new member to groups:
    - Compliance: https://gitlab.com/gitlab-org/govern/compliance
    - Compliance Frontend: https://gitlab.com/gitlab-org/govern/compliance/frontend
    - Compliance Backend: https://gitlab.com/gitlab-org/govern/compliance/backend

</details>

<details>
<summary>Security Policies</summary>

1. [ ] Manager: Add the new member to [Geekbot](https://app.geekbot.com/dashboard/standup/78587/view)
1. [ ] Manager: Add to appropriate Slack channels
    - Group Slack `#g_govern_security_policies`
    - Standup Slack `#g_govern_security_policies_standup`

</details>

</details>

<details>
<summary>Secure</summary>

1. [ ] Manager: Add the new member to the corresponding team group in [https://gitlab.com/groups/gitlab-org/secure](https://gitlab.com/groups/gitlab-org/secure) as `Maintainer` (this is mostly used for mentions)
1. [ ] Manager: Add the new member to [https://staging.gitlab.com/groups/secure-team-test](https://staging.gitlab.com/groups/secure-team-test) as Maintainer
1. [ ] Manager: Add the new member to the [Secure Stage Calendar](https://calendar.google.com/calendar/r/settings/calendar/Z2l0bGFiLmNvbV9tZDBhbzM2Z3B2bDV2MWY0MTI4ZXJobmo2Z0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) and explain what it is.
1. [ ] Manager: Add the new team member to the [team based Google Group](https://groups.google.com/my-groups)
1. [ ] Manager: Add the new member to [https://gitlab.com/groups/gitlab-org/secure/-/group_members](https://gitlab.com/groups/gitlab-org/secure/-/group_members)
1. [ ] Manager: Add the new member to the corresponding team's subgroup:
    - Composition Analysis: https://gitlab.com/gitlab-org/secure/composition-analysis-be as `Developer`
    - Dynamic Analysis: https://gitlab.com/gitlab-org/secure/dynamic-analysis as `Maintainer`
    - Static Analysis: https://gitlab.com/gitlab-org/secure/static-analysis-be as `Developer`
    - Secure Frontend: https://gitlab.com/gitlab-org/secure/frontend as `Maintainer`

</details>

#### Development Setup

##### Docker
1. [ ] Install [a Docker Desktop alternative](https://about.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop)
    1. [ ] If a more official recommendation has been made, open a Merge Request to update this template.

##### GitLab Development Kit

You can choose to either or both of these. Note that GCK runs better on Linux than macOS.

1. [ ] [GDK (Gitlab Development Kit)](https://gitlab.com/gitlab-org/gitlab-development-kit/-/tree/main) provides a standard development environment commonly used by Gitlab engineers.

    <details>
    <summary>GDK</summary>

    1. [ ] GDK
        1. [ ] Follow the installation instructions in the [index file](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md).
        1. [ ] Follow the instructions to [setup hostname and local interface](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/local_network.md).
        1. [ ] Visit `http://<gdk-hostname>:3000/users/sign_in` and login as an admin user with username `root` and password `5iveL!fe`. You will need to change the password after your first login.
        1. [ ] Upload your Ultimate license key for GitLab Enterprise Edition by visiting `http://<gdk-hostname>:3000/admin/application_settings/general` and going to `Add License`. If you do not have GitLab Ultimate license, follow instructions [here](https://handbook.gitlab.com/handbook/engineering/developer-onboarding/#working-on-gitlab-ee-developer-licenses) to obtain a license file.
        1. [ ] If you’re stuck, ask for help in `#development`, `#gdk` Slack channels. Use Slack search for your questions.
               first with filter `in:#gdk`. There is a possibility that someone has already had a similar issue.
        1. [ ] Check [How to use GitLab Development Kit doc](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/main/doc/howto/index.md)
        1. [ ] You’ll need to know [commands](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/main/HELP) to operate your local environment successfully. For example, to start database locally (to run tests), you need to start db with `gdk start postgresql redis`. For our purposes, run everything excluding the runners with `gdk start && gdk stop runner` because we'll run those separately in the steps below.
        1. [ ] GDK contains a collection of resources that helps with running an instance of GitLab locally as well as the
               GitLab codebase itself via a git remote. The GitLab code can be found in `/gitlab` folder of GDK. Check this folder.
        1. [ ] If you have not already, create a reminder for 3 months out from your start date to add yourself as a reviewer for the gitlab project by adding a [`projects` key](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/ea26398651a41e9eae631efdb266a4b9954e910f/data/team.yml#L20128-20129) to your entry in gitlab.com's `data/team.yml`. This will allow you to be automatically suggested as a reviewer for MRs.
    1. [ ] Runner
        1. [ ] Configure Runner with GDK (Recommended)
           1. [ ] Install GitLab Runner locally with [this tutorial](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/main/doc/howto/runner.md#using-gitlab-runner-with-gdk). Be sure to follow the [Docker related setup instructions](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/main/doc/howto/runner.md#executing-a-runner-from-within-docker).
        1. [ ] Configure Runner without GDK (Optional)
           1. [ ] You can [setup your Runners to run inside containers](https://docs.gitlab.com/runner/install/docker.html#docker-image-installation-and-configuration)

    </details>


2. [ ] [GCK (Gitlab Compose Kit)](https://gitlab.com/gitlab-org/gitlab-compose-kit) is an alternative development environment for those familiar with Docker. It works better for Linux than macOS.
    1. [ ] Full installation instructions for the GCK can be found [here](https://gitlab.com/gitlab-org/gitlab-compose-kit#use-it).
    1. [ ] The GCK has an automatically configured Gitlab Runner built in, but this might not cooperate with your firewall settings. Consider joining the [#gck](https://app.slack.com/client/T02592416/CDPA9TK1B) slack channel if you require assistance.

##### Editors and IDEs

We encourage everyone to use the appropriate tools for the task at hand. Since writing is a key part of everyone's work here, choosing the right editor is essential.

1. [ ] Take the time to choose and set up the [editors and IDEs](https://handbook.gitlab.com/handbook/tools-and-tips/editors-and-ides/) that work best for you. We also offer [Jetbrains individual IDEs](https://handbook.gitlab.com/handbook/tools-and-tips/editors-and-ides/jetbrains-ides/individual-ides/) available with [licenses](https://handbook.gitlab.com/handbook/tools-and-tips/editors-and-ides/jetbrains-ides/licenses/), so you can choose the one that suits you best.

### Day 7: Sec playground
Before proceeding with this step, you should have on your local machine:

 * a running instance of GitLab EE (via `gdk start`)
 * a working GitLab CI Runner connected to your local instance.

#### General GitLab Development
1. [ ] [Familiarize yourself with Sentry](https://docs.google.com/presentation/d/1j1J4NhGQEYBY8la6lCK-N-bw749TbcTSFTD-ANHiels/edit). Login and search for 500 errors related to your Stage.
1. [ ] GitLab uses Grafana for monitoring and observability. Review the [error budgets](https://handbook.gitlab.com/handbook/engineering/error-budgets/) handbook page and take a look out your groups [dashboard](https://dashboards.gitlab.net/dashboards/f/stage-groups/stage-groups).

<details>
<summary>Govern</summary>

<details>
<summary>Threat Insights</summary>

  1. [ ] [Watch the Security Report Parsing and Ingestion Brown Bag](https://youtu.be/1NhjpXw0UFI)
  1. [ ] [Read the Security report ingestion overview](https://docs.gitlab.com/ee/development/sec/security_report_ingestion_overview.html)
  1. [ ] Create special case vulnerabilities (expand section below)


<details>
<summary>Special case vulnerabilities</summary>

Sometimes one needs a vulnerability that can be resolved by a Merge Request on the vulnerability
page; unfortunately not all vulnerabilities have this capability. Here is a way to produce the
necessary data to use this feature.

1. [ ] Clone the [Yarn Remediation](https://staging.gitlab.com/secure-team-test/yarn-remediation) repo
1. [ ] Run a pipeline on the `cureable` branch.
1. [ ] Navigate to the project security dashboard select a vulnerability and view the details page. The
   `Resolve with MR` button should be available.
1. [ ] Click on the `Resolve with MR` button. A Merge Request is created and you are navigated to that page.

</details>

</details>

<details>
<summary>Security Policies</summary>

1. [ ] Read [Approval Rules development guide](https://docs.gitlab.com/ee/development/merge_request_concepts/approval_rules.html).
1. [ ] For each project ensure that you are creating project in group with Ultimate license enabled.

#### Security Reports

1. [ ] Fork or [import by URL](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html) [Security Reports Examples](https://gitlab.com/gitlab-examples/security/security-reports/) repository
1. [ ] Run the pipeline on the default branch.
1. [ ] Take a look at [Security tab](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/pipeline.html) in the Pipeline view and look at [Vulnerability Report](https://gitlab.com/help/user/application_security/vulnerability_report/).
1. [ ] Check the `.gitlab-ci.yml` file to see how security reports are generated based on the configuration.

#### Scan Execution Policies

1. [ ] Create new project or new group.
1. [ ] Go to `Security & Compliance` -> `Policies` page and click `New Policy`.
1. [ ] Select `Scan execution policy`.
1. [ ] Create policy with 2 rules:
    1. With rule to run for pipeline for every branch.
    1. With rule to run scheduled scans every day.
    1. Add one action to require `Container Scanning` scan.
1. [ ] Click `Configure with a merge request` and merge created MR.
1. [ ] Go to your project (or create new project if you have created the group).
1. [ ] Add `.gitlab-ci.yml` file with simple configuration:
   ```
   variables:
     DOCKER_IMAGE: nginx:1.18.0

   test-job:
     script:
     - echo "This is a test job."
   ```
1. [ ] Observe that new `container-scanning-0` job was added automatically to your pipeline.
1. [ ] Observe that scheduled scan was added with `container-scanning-0` job at given time.

#### Scan Result Policies

##### Scan Finding Approval Policies

1. [ ] Create new project or new group.
1. [ ] Go to `Security & Compliance` -> `Policies` page and click `New Policy`.
1. [ ] Select `Scan result policy`.
1. [ ] Create policy with one rule to require approval if all scanners find more than 0 critical newly detected vulnerabilities in all protected branches.
1. [ ] Create action to require approval from 1 approver (group or user that is a member of your project).
1. [ ] Click `Configure with a merge request` and merge created MR.
1. [ ] Go to your project (or create new project if you have created the group).
1. [ ] Add `.gitlab-ci.yml` file with simple configuration:
   ```
   variables:
     DOCKER_IMAGE: nginx:1.18.0

   include:
   - template: Security/Container-Scanning.gitlab-ci.yml
   ```
1. [ ] Modify `.gitlab-ci.yml` file and create new MR:
   ```
   variables:
     DOCKER_IMAGE: nginx:1.17.10

   include:
   - template: Security/Container-Scanning.gitlab-ci.yml
   ```
1. [ ] Verify if approval is required.

##### License Approval Policies

1. [ ] Fork or import by URL [license-approval-policies-test-cases](https://gitlab.com/gitlab-org/govern/demos/license-approval-policies-test-cases).
1. [ ] Enable `license_scanning_policies` [feature flag](https://docs.gitlab.com/ee/development/feature_flags/) for this project.
1. [ ] Follow the instructions from [Reports - how to test them](https://gitlab.com/gitlab-org/govern/demos/license-approval-policies-test-cases/-/blob/main/README.md#reports-how-to-test-them) section.

</details>

</details>

<details>
<summary>Secure</summary>

1. [ ] [Review the Secure Data Model Brown Bag](https://gitlab.com/gitlab-org/secure/brown-bag-sessions/-/issues/5)
2. [ ] [Watch the follow-up code walk-through](https://www.youtube.com/watch?v=iiYrvEPT5Fo).
3. [ ] [Watch the Security Report Parsing and Ingestion Brown Bag](https://youtu.be/1NhjpXw0UFI)

#### Security Reports Overview
Importing and running CI pipelines on this sample project will populate your security report dashboard.

1. [ ] On your local instance of GitLab, [import by URL](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html) the [Security Reports examples](https://gitlab.com/gitlab-examples/security/security-reports) repository. You may need to generate a [Personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) in your GitLab.com account with the `read_repository` scope, provide the credentials in the **Username** and **Password** optional fields, and use the `https://` Git URL.
1. [ ] Add this project to a [group](https://docs.gitlab.com/ee/user/group/#add-projects-to-a-group).
1. [ ] Kick off the CI process (pipeline) by following the [project README](https://gitlab.com/gitlab-examples/security/security-reports/blob/master/README.md).
    1. Navigate to the project page and select `Build` > `Pipelines`
    1. Click the `run pipeline` button
    1. Ensure the right branch is selected, then click the `run pipeline` button again
1. [ ] Check the `Security` tab of the pipeline results, where you should see a list of vulnerabilities. Note that in this scenario, this page is being populated using existing report JSON files so the file references in the vulnerability detail modal might be broken.
1. [ ] Inspect the `.gitlab-ci.yml` file and understand how the project is configured to generate reports. Feel free to dig into the example projects referenced within the file.

#### Secret Detection
<details>
<summary>Click to expand/contract</summary>

**Team Information**
1. Read the Secret Detection [Group Page](https://about.gitlab.com/direction/secure/secret-detection/) for more information about our team's focus and goals.
1. Read the Secret Detection [Design Page](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/secret_detection/) for more information about how secret detection is designed.

**More Practice with Security Report Generation**

1. [ ] On your local instance of GitLab, [import by URL](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html) the [test Secrets project](https://gitlab.com/gitlab-org/security-products/tests/secrets).
1. [ ] Add this project to a [group](https://docs.gitlab.com/ee/user/group/#add-projects-to-a-group). It will allow you to enable the Security dashboard correctly.
1. [ ] Create and run a pipeline for the `master` branch of the imported project.
1. [ ] Learn more about [SAST environment variables](https://docs.gitlab.com/ee/user/application_security/sast/#available-cicd-variables) and how they affect the analyzers.
1. [ ] Once you have a green pipeline, you should be able to see that a security report has been uploaded as an artifact (i.e. `gl-sast-report.json`. This report will be used to populate the Security dashboard.
1. [ ] Explore the `Security` tab in the pipeline page.
       When clicking on a vulnerability, GitLab shows a modal window with more information (like the file
       where the vulnerability has been found) and actions (dismiss and create an issue).
       Note: there must be an existing security report artifact generated by the
       pipeline in order for that tab to exist. Artifacts are generally deleted
       after some time, so old pipelines won't have a `Security` tab.

**Secret Detection Analyzer**

1. Read about Secret Detection's [three methods for detecting secrets](https://docs.gitlab.com/ee/user/application_security/secret_detection/).
1. Make sure you complete the [Auto DevOps](#autodevops) section below to enable the analyzer. Read more about the analyzer [here](https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/index.html#enable-the-analyzer).
1. The regex used to detect secrets can be found within `gitleaks.toml` in your `gitlab-org` project if you ever need to create a secret to trigger secret detection.

**Secret Push Protection Setup**

1. Watch the Introduction to Secret Push Protection YouTube [playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KoADm-g2vxfyR0m6QLphTv-).
1. Follow these instructions to [enable secret push protection](https://docs.gitlab.com/ee/user/application_security/secret_detection/secret_push_protection/index.html#enable-secret-push-protection).


</details>


#### Static Analysis
<details>
<summary>Click to expand/contract</summary>

**Local Setup**

1. [ ] On your local instance of GitLab, [import by URL](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html) a [test SAST project](https://gitlab.com/gitlab-org/security-products/tests?tag=SAST) of your choosing.
1. [ ] Add this project to a [group](https://docs.gitlab.com/ee/user/group/#add-projects-to-a-group). It will allow you to enable the Security dashboard correctly.
1. [ ] Create and run a pipeline for the `master` branch of the imported project.
1. [ ] Learn more about [SAST environment variables](https://docs.gitlab.com/ee/user/application_security/sast/#available-cicd-variables) and how they affect the analyzers.
1. [ ] Once you have a green pipeline, you should be able to see that a security report has been uploaded as an artifact (i.e. `gl-sast-report.json`. This report will be used to populate the Security dashboard.
1. [ ] Explore the `Security` tab in the pipeline page.
       When clicking on a vulnerability, GitLab shows a modal window with more information (like the file
       where the vulnerability has been found) and actions (dismiss and create an issue).
       Note: there must be an existing security report artifact generated by the
       pipeline in order for that tab to exist. Artifacts are generally deleted
       after some time, so old pipelines won't have a `Security` tab.

**Reviewing**

While you may not be ready yet, consider setting a reminder to update your team .yml file (found at https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/team_members/person) to be a reviewer for a [few of our projects](https://gitlab.com/explore/projects?tag=GL-Secure,SAST) within your first month. The `brakeman` and `sobelow` analyzers are good options to start with, but also consider asking your onboarding buddy. Also, be aware that some of the analyzers in the list above are in Maintenance mode and will not be good options.

**Additional Resources**

<details><summary>Click to expand</summary>

####  Category Direction

- SAST
	- [SAST Category Direction](https://about.gitlab.com/direction/secure/static-analysis/sast/)
- Secret Detection
	- [Secret Detection Category Direction](https://about.gitlab.com/direction/secure/static-analysis/secret-detection/ )
- Code Quality
	- [Code Quality Category Direction](https://about.gitlab.com/direction/secure/static-analysis/code_quality/)

#### VET (Vulnerability Extraction Tool)
- [Project Page](https://gitlab.com/gitlab-org/security-products/vet/vet#vet-vulnerability-extraction-tool)
- [Wiki](https://gitlab.com/groups/gitlab-org/security-products/vet/-/wikis/home)
- [VET Horizon 3](https://gitlab.com/groups/gitlab-org/-/epics/8600)

#### SAST Rule Management
- [SAST Rule Management and Deployment](https://docs.google.com/presentation/d/1JZQBv5g_MEmXhcuA4BmfssLgKiS6hQUdbYYSmGdSq5A/edit#slide=id.ge9bed85eb5_0_1)
- [Coordinating SAST Rule migrations,synchronizations, and refinements between Static Analysis and Vulnerability Research](https://docs.google.com/presentation/d/1X0YQwFM5OYFhtkOfkv7vMyTjgXI7qvSfTjR39Q3rFm0/edit#slide=id.g123a13deda8_0_405)
</details>

</details>

#### DAST
<details>
<summary>Click to expand/contract</summary>

GitLab's dynamic analysis tooling is used to find vulnerabilities in running websites.

**Viewing DAST vulnerabilities in the Security Dashboard**
<details>
<summary>Steps</summary>

Assumed: Docker installed, and you are comfortable using it, GDK started, license is installed and a runner configured and working.

1. [ ] Create a new project
1. [ ] From the project home page, click the `Web IDE` button. Click the `+` on the left pane for new file, and create a `.gitlab-ci.yml` file. It should have the following contents:

    ```yml
    stages:
      - build

    dast:
       stage: build
       image: alpine:3.11.3
       script:
       - apk add curl
       - curl https://gitlab.com/gitlab-examples/security/security-reports/-/raw/main/samples/dast-with-details.json > gl-dast-report.json
       artifacts:
         reports:
           dast: gl-dast-report.json
    ```

1. [ ] Click `Commit`. Type 'Add GitLab CI configuration' as the message, ensure you commit to `master`, and click `Commit` again.
1. [ ] In your project home page, click `Build -> Pipelines`. You should have a new Pipeline running your new build/dast job.
1. [ ] When the Pipeline has completed successfully, click on `Secure -> Security Dashboard`. You should see a vulnerability in `dast-with-details.json` in the Security Dashboard.

</details>

**Setup Browserker**
<details>
<summary>Steps</summary>

Before running Browserker:
1. [ ] Install Go, `asdf install golang <version> && asdf reshim golang`. The current version can be obtained by looking in the `.tool-versions` file. You may need to reshim after running Browserker make commands such as `make fmt-go`, `make lint-go`, `make di`, and `make mock.generate` for the first time.
2. [ ] Install Docker.
3. [ ] Install Buildx. It should be installed along with Docker on Linux, on Mac OS:
    ```
    $ brew install docker-buildx
    $ ln -sfn /opt/homebrew/bin/docker-buildx ~/.docker/cli-plugins/docker-buildx
    ```
4. [ ] Install [Chromium](https://download-chromium.appspot.com/), use the links at the bottom of the page to install the version for the architecture of your machine (e.g. Chromium for Mac ARM). Follow [this](https://www.howtogeek.com/803598/app-is-damaged-and-cant-be-opened/) in case Mac OS says the file is damaged when trying to run.
5. [ ] Install bash_unit. On Mac OS: `brew install bash_unit`
6. [ ] Install jq. On Mac OS: `brew install jq`
7. [ ] Create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) that has read permissions on the GitLab API and Container Registry.
8. [ ] Log in using Docker, `docker login registry.gitlab.com`. Supply your GitLab username and personal token created in the previous step.
9. [ ] Ensure you have a working python3 installed `/usr/bin/env python`. Python is used to generate config files, which is useful for debugging end-to-end tests.

Now setup Browserker:
1. [ ] Clone the [Browserker project](https://gitlab.com/gitlab-org/security-products/analyzers/browserker).
1. [ ] Try building the docker image using `GITLAB_API_TOKEN=<personal-access-token> make build.image`
1. [ ] Try running the tests using `GITLAB_API_TOKEN=<personal-access-token> make test-checks`
</details>

**Setup ApiFuzzing**
<details>
<summary>Steps</summary>

This is the setup guide for development tools and environment.

### IDE
1. [ ] Install an IDE of your choice:
   - [ ] JetBrains Rider:
     - Provides the best a cross-platform developer experience.
     - GitLab has a [license server](https://handbook.gitlab.com/handbook/tools-and-tips/editors-and-ides/jetbrains-ides/licenses/).
   - [ ] Visual Studio Code configured for C# and Python:
     - [C# Documentation](https://code.visualstudio.com/docs/languages/csharp)
     - [Python Documentation](https://code.visualstudio.com/docs/languages/python)
   - [ ] Visual Studio Community (for Windows OS).
1. [ ] Verify you can build the C# solution and start a debugging session.

### dotnet 8 SDK
1. [ ] Install .NET 8.0 SDK:
   - [Download .NET 8.0](https://dotnet.microsoft.com/en-us/download/dotnet/8.0)
1. [ ] Add the SDK installation path to the `PATH` environment variable to make it accessible from any CLI.
1. [ ] Configure your IDE:
   - For JetBrains Rider, add the path to:
     `Settings -> Build, Execution, Deployment -> Toolset and Build -> .NET Core CLI executable path`.

### Python v3.10
1. [ ] Install Python v3.10:
   - Use [asdf](https://asdf-vm.com/):
     - [asdf-python plugin](https://github.com/asdf-community/asdf-python)
     - The project includes an asdf `.tool-versions` file for the required version.

### Docker
1. [ ] Install Docker, but **not Docker Desktop**. Refer to the [GitLab Handbook](https://handbook.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop) for more details.

### Developer Tools
1. [ ] Read about [our developer tools](https://gitlab.com/gitlab-org/security-products/analyzers/api-fuzzing-src/-/tree/master/tools/dev-tools).
1. [ ] Install the most useful tools:
   - `testenv` -- Switch between test environments.
   - `targets` -- Pull/start various test targets.
1. [ ] Add `tools/dev-tools` to your `PATH`.
1. [ ] Optionally, set up `gitlab-apifuzzing-api` (located in `web/SDK/libraries/python`).

### Integration Tests
1. [ ] Follow the [README guide](https://gitlab.com/gitlab-org/security-products/analyzers/api-fuzzing-src/-/tree/master/tools/dev-tools) to set up and run integration tests.

</details>

**DAST Repositories**
<details>
<summary>Information</summary> 

1. [ ] [View this document](https://docs.google.com/document/d/17d15ZG49RTOrGpsPAroEpZMdWSkTLw6d9grsbW5lAxQ/edit?usp=sharing) and recording for a walkthrough of the DAST repositories.

</details>
</details>

#### Composition Analysis

1. [ ] [Feature Overview video summary](https://www.youtube.com/watch?v=mZvehoa4JdU&list=PL05JrBw4t0Kq7yUrZazEF3diazV29RRo1&index=7&t=0s). Contents include: Dependency Scanning, Container Scanning, License Management, Vulnerability DB, Analyzers, and data sources (DB and artifacts).

**Container Scanning**
<details>
<summary>Pre req</summary>

1. [ ] Read the [Container Scanning docs](https://docs.gitlab.com/ee/user/application_security/container_scanning/)
1. [ ] Ensure that your gitlab-runner is setup and running.
1. [ ] Set up [GDK container registry](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/registry.md#set-up-pushing-and-pulling-of-images-over-http)
    - [ ] Take note of the [Airplay receiver configuration](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/registry.md#check-airplay-receiver-process-on-macos) needed if you are on MacOS.
</details>

<details>
<summary>Steps</summary>

1. [ ] Create a new project that contains a Dockerfile based on this template. The alpine:3.7 image contains a known vulnerability.
    ```
    FROM alpine:3.7
    LABEL maintainer=GitLab

    ENTRYPOINT []
    ```

1. [ ] Create `.gitlab-ci.yml` using this [template](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/registry.md#running-container-scanning-on-a-local-docker-image-created-by-a-build-step-in-your-pipeline).
    - Once you commit this file, you should see a pipeline created for the build and test stage. Once the jobs are completed, you should see a security tab in the pipelines page with the vulnerabilities found.
</details>

#### Threat Insights

<details>
<summary>Click to expand/contract</summary>

##### Create special case vulnerabilities

Sometimes one needs a vulnerability that can be resolved by a Merge Request on the vulnerability
page; unfortunately not all vulnerabilities have this capability. Here is a way to produce the
necessary data to use this feature.

1. [ ] Clone the [Yarn Remediation](https://staging.gitlab.com/secure-team-test/yarn-remediation) repo
1. [ ] Run a pipeline on the `cureable` branch.
1. [ ] Navigate to the project security dashboard select a vulnerability and view the details page. The
   `Resolve with MR` button should be available.
1. [ ] Click on the `Resolve with MR` button. A Merge Request is created and you are navigated to that page.

</details>

#### AutoDevOps

GitLab can automatically configure the entire CI/CD pipeline for projects (without the need to create the `.gitlab-ci.yml` file), this feature is known as [AutoDevOps](https://docs.gitlab.com/ee/topics/autodevops/index.html)
As part of this task you need to:
1. [ ] Create a new project on GitLab.com and create Kubernetes cluster for your project by following this [guide](https://docs.gitlab.com/ee/topics/autodevops/cloud_deployments/auto_devops_with_gke.html#create-a-kubernetes-cluster), your GitLab google account should already have access to the `group-secure` [GCP](https://cloud.google.com) project; create a cluster in the [`group-secure`](https://console.cloud.google.com/home/dashboard?project=group-secure-a89fe7) project.
1. [ ] Install the required Kubernetes plugins as mentioned in the documentation linked above.
1. [ ] In your project go to ` Settings > CI/CD > Auto DevOps` and enable it. This should automatically spawn a new pipeline.
1. [ ] Make sure your pipeline (`CI/CD > Pipelines`) has an `Auto DevOps` label on it, it has all the Secure section jobs in it and that it eventually passes.

</details>

#### Prepare to contribute

1. [ ] Configure signing your commits [with SSH](https://docs.gitlab.com/ee/user/project/repository/signed_commits/ssh.html) or [with GPG](https://docs.gitlab.com/ee/user/project/repository/signed_commits/gpg.html).
1. [ ] Create an MR with improvements to this [document](https://gitlab.com/gitlab-org/secure/onboarding/blob/master/.gitlab/issue_templates/Technical_Onboarding.md).
1. [ ] Note the existence and location of the [security issue workflow](https://about.gitlab.com/handbook/engineering/workflow/#security-issues). You are unlikely to work on a security fix as your first task, but it's important to know that security MRs have a different workflow.
1. [ ] Record a quick demo video of a GitLab feature or of a development tip/trick that you use. Post the video to [GitLab unfiltered](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#post-everything) or upload it to Google Drive, and share a link to the video on [`#sec-section`](https://gitlab.enterprise.slack.com/archives/C02087FTL5V).
1. [ ] Read the [Sec Section development documentation](https://docs.gitlab.com/ee/development/sec/), as it contains a great wealth of information that helps understand the mechanics of how our code works.
1. [ ] Read the [Database Lab](https://docs.gitlab.com/ee/development/database/database_lab.html) docs. We use this frequently when working on query design and optimization.
    1. [ ] Also read [Understanding EXPLAIN plans](https://docs.gitlab.com/ee/development/database/understanding_explain_plans.html) to interpret the output from the Database Lab.
    1. [ ] Using the [performance bar](https://docs.gitlab.com/ee/administration/monitoring/performance/performance_bar.html), visit [vulnerability report](https://gitlab.com/gitlab-org/gitlab/-/security/vulnerability_report) or [dependencies list](https://gitlab.com/gitlab-org/gitlab/-/dependencies) page on `gitlab.com/gitlab-org/gitlab`. Toggle the performance bar dropdown to understand the GraphQL APIs those pages are making.
    1. [ ] Identify a few GraphQL API queries from the performance bar which are taking longer and find the underlying PG queries (results sorted by duration). The [GraphQL development guide](https://docs.gitlab.com/ee/development/graphql_guide) may be helpful and there is also this [deep dive](https://www.youtube.com/watch?v=-9L_1MWrjkg) on GraphQL.
    1. [ ] Take those top 3 to 5 queries and run an explain plan on DB lab, understand the query plan, understand the various [visualizing tools](https://docs.gitlab.com/ee/development/database/understanding_explain_plans.html#database-lab-engine) available, indices those queries are using, and compare [cold cache vs warm cache](https://docs.gitlab.com/ee/development/database/query_performance.html#cold-and-warm-cache) results of these queries.
1. [ ] Locally, or in a verification project, set up a SAST scanner and explore our vulnerability tooling. [This was a Key Result in FY24-Q2](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/6947) and you can use this to guide you through the process.
    1. [ ] Write up your learnings on that Key Result.

#### Understand GitLab Infrastructure Better
1. [ ] [Review GitLab Monitoring Tools and test them out](https://www.youtube.com/playlist?list=PL05JrBw4t0KpQMEbnXjeQUA22SZtz7J0e)
1. [ ] [Review Visualization Tools and test them](https://www.youtube.com/playlist?list=PL05JrBw4t0KrDIsPQ68htUUbvCgt9JeQj)
If you can not access the above playlists because you get 'This playlist is private.', go to the top right of the screen in Youtube and click 'Switch Account' > 'GitLab Unfiltered'

### Now you are ready for new tasks
You're awesome!

## Troubleshooting

### Security Dashboard won't show vulnerabilities

TLDR: Re-run the pipeline on the default branch

There is currently a distinction between the data that populates the project
security dashboard (PSD) and group security dashboard (GSD).

The PSD pulls vulnerabilities directly from the last pipeline on the default branch
(usually `master`). This requires the last pipeline to have been ran and to have
uploaded a security report.

The GSD pulls vulnerabilities directly from the database, as `Vulnerabilities::Occurrence`
records. This requires the last pipeline to have been ran on the default branch
and been processed through our CI parsers.
