# Onboarding Issue: SAST in the IDE

The static analysis team supports SAST in the IDE features with SaaS real-time scans.

## Architecture

- [ ] Read the initial [architecture design document](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/sast_ide_integration/).
- [ ] Read about the [scanner service](https://gitlab.com/gitlab-org/secure/sast-scanner-service#sast-scanner-service).
  - [ ] Read about [Runway](https://handbook.gitlab.com/handbook/engineering/infrastructure/platforms/tools/runway/), the platform that hosts the scanner service.
  - [ ] Read about [Cloud Connector](https://docs.gitlab.com/ee/development/cloud_connector/architecture.html), the framework that simplifies the connection from GitLab instances to the service.
  - [ ] [Optional] Take a look at the [GitLab rails](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/lib/api/security_scans.rb) code that implements the [real-time scan endpoint](https://docs.gitlab.com/ee/api/projects.html#real-time-security-scan).
- [ ] Read about the [benchmarking suite](https://gitlab.com/gitlab-org/secure/sast-ide-benchmark#sast-ide-benchmark) for evaluating service performance.
- [ ] Read about GitLab's IDE features.
  - [ ] Read about the [GitLab Language Server](https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp#introduction)
  - [ ] Read about [GitLab's Workflow extension for VS Code](https://gitlab.com/gitlab-org/gitlab-vscode-extension#gitlab-workflow).

## Development setup

- [ ] Read about the [SAST IDE Integration](https://gitlab.com/gitlab-org/secure/sast-ide-integration#sast-ide-integration) project
  - [ ] Read about how [deployment](https://gitlab.com/gitlab-org/secure/sast-ide-integration/-/blob/main/docs/runway.md) is done.
  - [ ] Read about how [benchmarking](https://gitlab.com/gitlab-org/secure/sast-ide-integration/-/blob/main/docs/benchmark.md?ref_type=heads) is done.
- [ ] Check out the integration project and run `scripts/setup.sh`.
- [ ] Run through each of the [debugging](https://gitlab.com/gitlab-org/secure/sast-ide-integration/-/blob/main/docs/debugging.md?ref_type=heads#debugging) scenarios.
  - [ ] [Run the scanner service locally](https://gitlab.com/gitlab-org/secure/sast-ide-integration/-/blob/main/docs/debugging.md?ref_type=heads#starting-the-scanner-service-locally).
    - [ ] [Test the service with `curl`](https://gitlab.com/gitlab-org/secure/sast-ide-integration/-/blob/main/docs/debugging.md?ref_type=heads#testing-the-scanner-service-with-curl).
  - [ ] [Run the scanner service through GDK](https://gitlab.com/gitlab-org/secure/sast-ide-integration/-/blob/main/docs/debugging.md?ref_type=heads#testing-the-scanner-service-with-gdk)
    - [ ] Test the API endpoint with `curl`.
- [ ] [optional] Run the scanner service through GDK and test from the IDE.
  - [ ] Debug the [VSCode extension](https://gitlab.com/gitlab-org/secure/sast-ide-integration/-/blob/main/docs/debugging.md?ref_type=heads#gitlab-workflow-extension).
  - [ ] Debug the [language server](https://gitlab.com/gitlab-org/secure/sast-ide-integration/-/blob/main/docs/debugging.md?ref_type=heads#gitlab-language-server).

## Tooling [optional]

- [ ] Read about [Git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules) and [their use in the integration project](https://gitlab.com/gitlab-org/secure/sast-ide-integration/-/blob/main/docs/submodules.md)
- [ ] Read about the [`lefthook`](https://docs.gitlab.com/ee/development/contributing/style_guides.html#pre-push-static-analysis-with-lefthook) Git hooks manager and [its use in the integration project](https://gitlab.com/gitlab-org/secure/sast-ide-integration/-/blob/main/lefthook.yml).
- [ ] Read about [Renovate](https://docs.renovatebot.com/) for dependency updating and [its use in the Scanner Service project](https://gitlab.com/gitlab-org/secure/sast-scanner-service/-/blob/main/renovate.json)
