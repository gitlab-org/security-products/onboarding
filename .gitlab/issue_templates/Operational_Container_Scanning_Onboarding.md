# Overview

Operational Container Scanning(OCS) enables our customers to scan container images in their Kubernetes cluster and triage the detected vulnerabilities in their Gitlab project.

Duplicate this issue and complete each section to get familiar with OCS.

# Topics

## Concepts (~2 days)

* [ ] Watch [OCS demo video](https://www.youtube.com/watch?v=\_4QFgNzW-wc)
* [ ] Watch [OCS internal workflow demo video](https://www.youtube.com/watch?v=Baqr7eDWris&feature=youtu.be)
* [ ] Read the [OCS user doc](https://docs.gitlab.com/ee/user/clusters/agent/vulnerabilities.html)
* Kubernetes
    * [ ] Watch video on [What is Kubernetes](https://www.youtube.com/watch?v=VnvRFRk_51k)
    * [ ] Read about the different types of [workload resources](https://kubernetes.io/docs/concepts/workloads/controllers/)
* Trivy
    * OCS uses Trivy to scan container images. It will be useful to get familiar with how Trivy works
        * [ ] Watch [demo video of how Trivy image scanning works](https://www.youtube.com/watch?v=-IH5inFyEqU)
    * Trivy has many sub commands, the sub command that OCS uses is the Trivy K8s command
        * [ ] Read the [trivy k8s docs](https://aquasecurity.github.io/trivy/v0.45/docs/target/kubernetes/)
        * [ ] [Optional] [Try running trivy k8s](https://aquasecurity.github.io/trivy/v0.45/tutorials/kubernetes/cluster-scanning/) in your k8s cluster if you already have one set up.
* Gitlab Agent for Kubernetes
    * OCS code is packaged as part of Gitlab Agent
        * [ ] Watch [demo video of how Gitlab Agent works](https://www.youtube.com/watch?v=GfOnS7r8QDI)
        * [ ] Read [Gitlab Agent architecture doc](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/architecture.md)
* OCS vs Container Scanning
    |  | OCS | Container Scanning |
    | ------ | ------ | ------ |
    | How it's setup | [Installation of Gitlab Agent of Kubernetes](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#installation-steps) | [Configure in gitlab-ci.yml file](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) |
    | How scans are triggered | Cron Schedule configured in [agent config](https://docs.gitlab.com/ee/user/clusters/agent/vulnerabilities.html#enable-via-agent-configuration) or [scan execution policies](https://docs.gitlab.com/ee/user/clusters/agent/vulnerabilities.html#enable-via-scan-execution-policies) | CI configuration in [gitlab-ci.yml file](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) or [scan execution policies](https://docs.gitlab.com/ee/user/application_security/policies/scan-execution-policies.html) |
    | How vulnerabilities are reported | Gitlab project vulnerabilities project page, Operational Vulnerabilities Tab | Gitlab project vulnerabilities project page, Development Vulnerabilities Tab |
    | Where does the code reside | [Gitlab Agent](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/tree/master) | [Container Scanning](https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning) |
    | Underlying image scanners | [Trivy K8s](https://aquasecurity.github.io/trivy/v0.45/docs/target/kubernetes/) | [Trivy image](https://aquasecurity.github.io/trivy/v0.45/docs/target/container_image/), [Grype](https://github.com/anchore/grype) |

## Setup(~1 day)

* [ ] [Setup OCS in your local machine](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/operational_container_scanning.md#running-ocs-locally)

## Code walkthrough(~2.5 days)
* **Configuration updates(~0.5 day)**
    * [ ] Updates can be applied from either `Agent config` or `Scan execution policies`. If both are present, `Scan execution policies` takes precedence over `Agent config`.
        * [Code that implements this](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/544d6c68675455b019eb116355971f3d5ac44e29/internal/module/starboard_vulnerability/agent/module.go#L54-65)
        * [Test that covers these use cases](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/8a1d4b457247e83d6e560b67e4f4224a3d4254ba/internal/module/starboard_vulnerability/agent/module_test.go#L332-387)
    * [ ] Scan execution policies are constantly being polled
        * Code chunks that implements this, [1](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/544d6c68675455b019eb116355971f3d5ac44e29/internal/module/starboard_vulnerability/agent/module.go#L66), [2](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/875d757d1968e9b9927b6f1d4e713d13b4684ac3/internal/module/starboard_vulnerability/agent/security_policies_worker.go)
    * [ ] Some organizations have really large images to scan which causes the scanner pod to [run out of memory](https://gitlab.com/gitlab-org/gitlab/-/issues/384238). 
        * As such, the user is able to [configure the resource requirements via the agent configuration](https://docs.gitlab.com/ee/user/clusters/agent/vulnerabilities.html#configure-scanner-resource-requirements).
        * This logic is somewhat complex [see this thread for the discussion](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/949#note_1386695579)
        * [Code that implements this](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/544d6c68675455b019eb116355971f3d5ac44e29/internal/module/starboard_vulnerability/agent/module.go#L42-79)
        * [Test that covers these use cases](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/8a1d4b457247e83d6e560b67e4f4224a3d4254ba/internal/module/starboard_vulnerability/agent/module_test.go#L332-387)
* **Starting the Trivy scanner pod(~0.5 day)**
    * [ ] When the configuration is parsed, a scheduler worker is started that runs the scanner at the specified time.
        * [Code that implements this](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/a16c5a3bcc721f98db2d6f2ba366a61a40c56ad6/internal/module/starboard_vulnerability/agent/worker.go#L34-47)
    * [ ] The scanner starts 1 Trivy scanner pod for each specified namespace
        * [Code that implements this](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/db7ecff6abd431c7e7887c72723548a9529f1a22/internal/module/starboard_vulnerability/agent/scanner.go#L130-154)
    *  [ ] The Trivy scanner pod uses the image reference from the [aquasecurity dockerhub registry](https://hub.docker.com/r/aquasec/trivy/) to specify which Trivy version to use.
        * Customers have requested to allow usage of custom Trivy images. However, we have to first handle the scenario where the image version used is compatible with our [parsing logic](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/a16c5a3bcc721f98db2d6f2ba366a61a40c56ad6/internal/module/starboard_vulnerability/agent/scanner.go#L82-96). More context and consideration captured in [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/389870#consideration).
        * [Code that implements this](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/db7ecff6abd431c7e7887c72723548a9529f1a22/internal/module/starboard_vulnerability/agent/scanner.go#L33)
* **Parsing the vulnerabilities from the Trivy scanner pod(~0.5 days)**
    * [ ] Currently, vulnerabilities are parsed from the logs of the Trivy scanner pod
        * This is not the most robust approach as there's a [max limit on logs to read](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/db7ecff6abd431c7e7887c72723548a9529f1a22/internal/module/starboard_vulnerability/agent/scanner.go#L336) to prevent the agent from crashing and the log format could change. 
        * This is an issue that was created to [consider an alternative approach](https://gitlab.com/gitlab-org/gitlab/-/issues/407649)
        * [Code that implements this](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/db7ecff6abd431c7e7887c72723548a9529f1a22/internal/module/starboard_vulnerability/agent/scanner.go#L82-96)
    *  [ ] Trivy scans takes an arbitrary time to run as it depends on the size of the image. As such, there's a need to watch when the Trivy scanner pod has completed the scan.
        * [Code that implements this](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/db7ecff6abd431c7e7887c72723548a9529f1a22/internal/module/starboard_vulnerability/agent/scanner.go#L210-279)
        * Testing this is difficult since the tests do not actually start a k8s cluster but instead uses the [Kubernetes fake](https://pkg.go.dev/k8s.io/client-go/kubernetes/fake) package. As such a [custom watcher is used](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/875d757d1968e9b9927b6f1d4e713d13b4684ac3/internal/module/starboard_vulnerability/agent/scanner_test.go#L529-572) to simulate the pod status changes.
        * [Explanation of how watch works in Kubernetes](https://kubernetes.io/docs/reference/using-api/api-concepts/#efficient-detection-of-changes)
    * Vulnerabilities from [managed pods](https://kubernetes.io/docs/concepts/overview/working-with-objects/owners-dependents/) are excluded as they are similar to the vulnerabilities of their owners. 
        * [Code that implements this](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/db7ecff6abd431c7e7887c72723548a9529f1a22/internal/module/starboard_vulnerability/agent/scanner.go#L282-300)
* **Sending vulnerabilities to Gitlab rails(~0.5 day)**
    * [ ] Once vulnerabilities are parsed, they are transmitted to Gitlab via API
        * [Code that implements this](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/f59017f95fcda1f052b85a85a56a10c59b288daf/internal/module/starboard_vulnerability/agent/reporter.go#L38-67)
    * [ ] Once all vulnerabilities are reported, there is a step to resolve no longer detected vulnerabilities. This will set the status of previously reported vulnerabilities in the project dashboard to `No longer detected`
        * [Code that implements this](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/db7ecff6abd431c7e7887c72723548a9529f1a22/internal/module/starboard_vulnerability/agent/scanner.go#L160) 
    * [ ] The UUID of vulnerabilities are determined by various parameters of the vulnerability and image being scanned. This makes up the fingerprint of the vulnerability. 
        * An OCS vulnerability fingerprint and UUID is defined in the Gitlab rails code([UUID](https://gitlab.com/gitlab-org/gitlab/-/blob/f50075762cf33d3841b88bb191770776b07ede77/ee/app/services/vulnerabilities/create_service_base.rb#L105-110), [fingerprint](https://gitlab.com/gitlab-org/gitlab/-/blob/f50075762cf33d3841b88bb191770776b07ede77/ee/app/services/vulnerabilities/starboard_vulnerability_create_service.rb#L62-71)).
        * The fields used to construct the fingerprint are set in the Gitlab Agent [here](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/f59017f95fcda1f052b85a85a56a10c59b288daf/internal/module/starboard_vulnerability/agent/report.go#L160-173)
        * It is **important** that we are mindful when changing these fields as it could cause vulnerabilities that are no longer detected to be flagged since its UUID has changed. Similarly, existing vulnerabilities would be marked as no longer detected.
        * Currently there are no tests to detect this scenario. The proposal for test implementation is tracked in this [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/407167).
* **Scanning private images(~0.5 days)**
    * [ ] [Private images are scanned](https://docs.gitlab.com/ee/user/clusters/agent/vulnerabilities.html#scanning-private-images) if the `image's` Pod Specification contains `imagePullSecret` or its `service account` contains an `imagePullSecret`.
        * See this [tutorial](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/) on how `imagePullSecrets` are created and configured.
        * The parsing of the secret is handled by `trivy k8s` [here](https://github.com/aquasecurity/trivy-kubernetes/blob/9492b07bcc3cedff43620f473271a023f7d0632e/pkg/k8s/k8s.go#L509C1-L527)
        *  PRs that added functionality for parsing private images: 
            * [PR for `trivy` codebase]( https://github.com/aquasecurity/trivy/pull/4987)
            * [PR for `trivy-kubernetes` codebase](https://github.com/aquasecurity/trivy-kubernetes/pull/197)

## Dependent code repositories(~1 day)
* [ ] [Trivy k8s](https://github.com/aquasecurity/trivy/tree/main/pkg/k8s). 
    * This contains the logic for the `trivy k8s` subcommand. It uses the `Trivy kubernetes` library to retrieve the images in the cluster that needs to be scanned.
    * [Code that implements this](https://gitlab.com/gitlab-org/security-products/dependencies/trivy/-/blob/0c8919e1e4bd32729e4afd697882deba528a3ada/pkg/k8s/commands/resource.go#L41)
* [ ] [Trivy Kubernetes](https://github.com/aquasecurity/trivy-kubernetes). 
    * Implements the logic that interfaces with the Kubernetes cluster to retrieve images from workloads as well as the private image credentials.

## Others(~0.5 day)
* [ ] [Release process](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/releases.md)